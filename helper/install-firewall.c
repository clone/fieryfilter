#include <sys/types.h>
#include <grp.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <glib.h>

int group_member(gid_t gid) {
    int n;
    gid_t *g;
    if ((n = getgroups(0, NULL)) < 0)
        return -1;

    g = g_new(gid_t, n);
    if (getgroups(n, g) < 0) {
        g_free(g);
        return -1;
    }

    for (; n >= 0; n--)
        if (g[n] == gid) {
            g_free(g);
            return 1;
        }

    g_free(g);
    return 0;        
}

int main(int argc, char *argv[]) {
    struct group *gr;
    
    if (!(gr = getgrnam("fieryfilter"))) {
        g_message("Could not find group fieryfilter\n");
        return 1;
    }

    if (group_member(gr->gr_gid) != 1) {
        fprintf(stderr, "I am sorry, you are not a member of the group \"fieryfilter\", access denied.\n");
        return 1;
    }

    if (geteuid() != 0) {
        fprintf(stderr, "Binary %s not SETUID.\n", argv[0]);
        return 1;
    }
    
    if (setuid(geteuid()) != 0) {
        fprintf(stderr, "Cannot make uid=euid: %s\n", strerror(errno));
        return 1;
    }
    
    if (setgid(0) != 0) {
        fprintf(stderr, "Cannot set uid=0: %s\n", strerror(errno));
        return 1;
    }

    if (setgroups(0, NULL) != 0) {
        fprintf(stderr, "setgroups(0, NULL): %s\n", strerror(errno));
        return 1;
    }

    if (execvp("xml-iptables-safe", argv) < 0) {
        fprintf(stderr, "Could not run xml-iptables-safe: %s\n", strerror(errno));
        return 1;
    }

    return 0;
}
