# -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

# $Id: configure.ac 61 2004-02-09 18:03:45Z lennart $

# This file is part of ivam2.
#
# ivam2 is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# ivam2 is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ivam2; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.


pkg_modules="glib-2.0 > 2.0 gtk+-2.0 >= 2.2.1 libxml-2.0 >= 2.0"
PKG_CHECK_MODULES(PACKAGE, [$pkg_modules])


AC_PREREQ(2.59)
AC_INIT([fieryfilter],[0.5],[mzsvrelsvygre (at) 0pointer (dot) de])
AC_CONFIG_SRCDIR([daemon/main.c])
AC_CONFIG_HEADERS([config.h])
AM_INIT_AUTOMAKE([foreign -Wall])

AC_SUBST(PACKAGE_URL, [http://0pointer.de/lennart/projects/ivam2/])

if type -p stow > /dev/null && test -d /usr/local/stow ; then
   AC_MSG_NOTICE([*** Found /usr/local/stow: default install prefix set to /usr/local/stow/${PACKAGE_NAME}-${PACKAGE_VERSION} ***])
   ac_default_prefix="/usr/local/stow/${PACKAGE_NAME}-${PACKAGE_VERSION}"
fi

# Checks for programs.
AC_PROG_CC
AC_PROG_LN_S
AC_PROG_MAKE_SET
AM_PATH_PYTHON

# If using GCC specifiy some additional parameters
if test "x$GCC" = "xyes" ; then
   CFLAGS="$CFLAGS -pipe -Wall"
fi

# Checks for header files.
AC_HEADER_STDC
AC_HEADER_SYS_WAIT
AC_CHECK_HEADERS([fcntl.h inttypes.h limits.h stddef.h stdlib.h string.h sys/socket.h sys/time.h termios.h unistd.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_TYPE_UID_T
AC_C_INLINE
AC_TYPE_MODE_T
AC_TYPE_PID_T
AC_TYPE_SIZE_T
AC_HEADER_TIME

# Checks for library functions.
AC_REPLACE_FNMATCH
AC_FUNC_FORK
AC_FUNC_MALLOC
AC_TYPE_SIGNAL
AC_FUNC_STAT
AC_CHECK_FUNCS([regcomp dup2 gettimeofday memset mkfifo rmdir setenv strchr strcspn strdup strerror strrchr strspn strtol])

PKG_CHECK_MODULES(LIBDAEMON, [ libdaemon >= 0.4 ])
AC_SUBST(LIBDAEMON_CFLAGS)
AC_SUBST(LIBDAEMON_LIBS)

PKG_CHECK_MODULES(GUILIBS, [ glib-2.0 >= 2.4 gtk+-2.0 >= 2.4 libxml-2.0 >= 2.0])
AC_SUBST(GUILIBS_CFLAGS)
AC_SUBST(GUILIBS_LIBS)

# Init script location
AC_ARG_WITH(initdir, AS_HELP_STRING(--with-initdir=DIR,Install init script in DIR (system dependent default)))

if test "x$with_initdir" = xno ; then
	SYSINITDIR=no
	AC_MSG_NOTICE([*** No init script will be installed ***])
else
	test "x$with_initdir" = xyes && with_initdir=

	if test "x$with_initdir" = x ; then
	    if test -d /etc/init.d ; then
        	SYSINITDIR=/etc/init.d
	    else
    		if test -d /etc/rc.d/init.d ; then
	        	SYSINITDIR=/etc/rc.d/init.d
		    else
	        	AC_MSG_ERROR([missing --with-initdir=DIR])
	    	fi
	    fi
	else
	   SYSINITDIR="$with_initdir"
	fi
	AC_MSG_NOTICE([*** Init script will be installed in $SYSINITDIR ***])
fi
AC_SUBST(SYSINITDIR)
AM_CONDITIONAL(NOINITSCRIPT, test x$SYSINITDIR = xno)

# Where to place the spool directory
AC_ARG_WITH(spooldir, AS_HELP_STRING(--with-spooldir=DIR,The $PACKAGE spool directory (/var/spool/$PACKAGE)))

if test "x$with_spooldir" = xyes -o "x$with_spooldir" = xno -o "x$with_spooldir" = x ; then
    SPOOLDIR="/var/spool/$PACKAGE"
else
    SPOOLDIR="$with_spooldir"
fi

AC_MSG_NOTICE([*** Spool directory is $SPOOLDIR ***])
AC_SUBST(SPOOLDIR)

# Where to place the UUCP lock directory
AC_ARG_WITH(lockdir, AS_HELP_STRING(--with-lockdir=DIR,The UUCP lock directory (/var/lock)))

if test "x$with_lockdir" = xyes -o "x$with_lockdir" = xno -o "x$with_lockdir" = x ; then
    LOCKDIR="/var/lock"
else
    LOCKDIR="$with_lockdir"
fi

AC_MSG_NOTICE([*** UUCP lock directory is $LOCKDIR ***])
AC_SUBST(LOCKDIR)

# LYNX documentation generation
AC_ARG_ENABLE(lynx,
        AS_HELP_STRING(--disable-lynx,Turn off lynx usage for documentation generation),
[case "${enableval}" in
  yes) lynx=yes ;;
  no)  lynx=no ;;
  *) AC_MSG_ERROR(bad value ${enableval} for --disable-lynx) ;;
esac],[lynx=yes])

if test x$lynx = xyes ; then
   AC_CHECK_PROG(have_lynx, lynx, yes, no)

   if test x$have_lynx = xno ; then
     AC_MSG_ERROR([*** Sorry, you have to install lynx or use --disable-lynx ***])
   fi
fi

AM_CONDITIONAL([USE_LYNX], [test "x$lynx" = xyes])

# XMLTOMAN manpage generation
AC_ARG_ENABLE(xmltoman,
        AS_HELP_STRING(--disable-xmltoman,Disable rebuilding of man pages with xmltoman),
[case "${enableval}" in
  yes) xmltoman=yes ;;
  no)  xmltoman=no ;;
  *) AC_MSG_ERROR([bad value ${enableval} for --disable-xmltoman]) ;;
esac],[xmltoman=yes])

if test x$xmltoman = xyes ; then
   AC_CHECK_PROG(have_xmltoman, xmltoman, yes, no)

   if test x$have_xmltoman = xno ; then
     AC_MSG_WARN([*** Not rebuilding man pages as xmltoman is not found ***])
     xmltoman=no
   fi
fi

AM_CONDITIONAL([USE_XMLTOMAN], [test "x$xmltoman" = xyes])

# gengetopt command line parser generation
AC_ARG_ENABLE(gengetopt,
        AS_HELP_STRING(--disable-gengetopt,Disable rebuilding of command line parser with gengetopt),
[case "${enableval}" in
  yes) gengetopt=yes ;;
  no)  gengetopt=no ;;
  *) AC_MSG_ERROR([bad value ${enableval} for --disable-gengetopt]) ;;
esac],[gengetopt=yes])

if test x$gengetopt = xyes ; then
   AC_CHECK_PROG(have_gengetopt, gengetopt, yes, no)

   if test x$have_gengetopt = xno ; then
     AC_MSG_WARN([*** Not rebuilding command line parser as gengetopt is not found ***])
     gengetopt=no
   fi
fi

AM_CONDITIONAL([USE_GENGETOPT], [test "x$gengetopt" = xyes])

AC_CONFIG_FILES([Makefile src/Makefile clients/Makefile ulaw/Makefile doc/Makefile conf/Makefile doc/README.html man/Makefile])
AC_OUTPUT

