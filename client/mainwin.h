#ifndef foomainwinhfoo
#define foomainwinhfoo

#include <gtk/gtk.h>

#include "connection.h"

GtkWidget* get_main_window(void);
void mainwin_show();
void mainwin_update_status_bar();

#endif
