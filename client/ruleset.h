#ifndef foorulesethfoo
#define foorulesethfoo

#include <glib.h>
#include <gtk/gtk.h>

#include "../daemon/common.h"
#include "main.h"
#include "connection.h"
#include "rule.h"

typedef struct ruleset {
    gboolean modified;
    gchar *filename;

    guint icmp_reject_code;
    gboolean use_tcp_rst;
    gboolean ignore_rules;
    verdict_t unmatch_verdict;

    GList *rules;
} ruleset_t;

extern ruleset_t ruleset;

int ruleset_new(gchar *fn);
int ruleset_load(gchar *fn);
int ruleset_save(gchar *fn);
int ruleset_initial_load();
int ruleset_commit();
int ruleset_append_rule(rule_t *rule);
int ruleset_update_rule(rule_t *rule);
int ruleset_remove_rule(rule_t *rule);
int ruleset_move_rule(rule_t *rule, int i);
void ruleset_clear();

void ruleset_widget_init();
void ruleset_update_ui();
void ruleset_show_rule_info();
rule_t* ruleset_get_current_rule();

int ruleset_install();

int ruleset_init();
void ruleset_done();



#endif
