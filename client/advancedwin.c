#include <gtk/gtk.h>

#include "advancedwin.h"
#include "interface.h"
#include "format.h"
#include "support.h"
#include "connection.h"
#include "ruleset.h"

static gboolean ignore_apply = TRUE;

static GtkWidget* get_window(void) {
    static GtkWidget *aw = NULL;

    if (!aw) {
        aw = create_advanced_window();
        fill_icmp_menu(GTK_OPTION_MENU(lookup_widget(aw, "icmp_option_menu")), TRUE, 0);
    }
    return aw;
}


void advancedwin_show(gboolean b) {
    if (b) {
        advancedwin_fill();
        gtk_widget_show_all(get_window());
        ignore_apply = FALSE;
    } else {
        ignore_apply = TRUE;
        gtk_widget_hide(get_window());
    }
}

void advancedwin_fill() {
    GtkWidget *aw = get_window();

    set_icmp_menu_entry(GTK_OPTION_MENU(lookup_widget(aw, "icmp_option_menu")), TRUE, ruleset.icmp_reject_code);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(lookup_widget(aw, "tcp_rst_check_button")), ruleset.use_tcp_rst);
}

void advancedwin_apply() {
   GtkWidget *w, *aw;
   GtkOptionMenu *om;
   
   if (ignore_apply)
        return;
    
   aw = get_window();
   om = GTK_OPTION_MENU(lookup_widget(aw, "icmp_option_menu"));
   
   w = GTK_WIDGET(g_list_nth_data(gtk_container_get_children(GTK_CONTAINER(gtk_option_menu_get_menu(om))),
                                                             gtk_option_menu_get_history(om)));
   ruleset.icmp_reject_code = GPOINTER_TO_UINT(g_object_get_data(G_OBJECT(w), "icmp_value"));
   
   ruleset.use_tcp_rst = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(lookup_widget(aw, "tcp_rst_check_button")));

   ruleset.modified = TRUE;
   ruleset_update_ui();
}
