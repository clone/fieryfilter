#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netdb.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"

#include "connection.h"
#include "mainwin.h"
#include "rulewin.h"
#include "log.h"
#include "rulewin.h"
#include "advancedwin.h"

gboolean on_connection_window_delete_event(GtkWidget *widget, GdkEvent *event, gpointer user_data) {
    conn_verdict(VERDICT_DROP);
    return TRUE;
}
                                                                                                                                                                                
                                                                                                                                                                                
void on_drop_button_clicked(GtkButton *button, gpointer user_data) {
    conn_verdict(VERDICT_DROP);
}
                                                                                                                                                                                
                                                                                                                                                                                
void on_reject_button_clicked(GtkButton *button, gpointer user_data) {
    conn_verdict(VERDICT_REJECT);
}
                                                                                                                                                                                
                                                                                                                                                                                
void on_accept_button_clicked(GtkButton *button, gpointer user_data) {
    conn_verdict(VERDICT_ACCEPT);
}

void on_quit_activate(GtkMenuItem *menuitem, gpointer user_data) {
    gtk_main_quit();
}

void on_clear_log_activate(GtkMenuItem *menuitem, gpointer user_data) {
    log_widget_clear();
}

void on_sticky_button_toggled(GtkToggleButton *togglebutton, gpointer user_data) {
    conn_set_sticky();
}

gboolean on_main_window_delete_event (GtkWidget *widget, GdkEvent *event, gpointer user_data) {
    gtk_main_quit();
    return FALSE;
}

void on_icmp_option_menu_changed(GtkOptionMenu *optionmenu, gpointer user_data) {
    advancedwin_apply();
}

void on_direction_optionmenu_changed(GtkOptionMenu *optionmenu, gpointer user_data) {
    rulewin_set_sensitive(GTK_WIDGET(optionmenu));
}

void on_match_direction_checkbutton_toggled(GtkToggleButton *togglebutton, gpointer user_data) {
    rulewin_set_sensitive(GTK_WIDGET(togglebutton));
}


void on_match_interfaces_checkbutton_toggled(GtkToggleButton *togglebutton, gpointer user_data) {
    rulewin_set_sensitive(GTK_WIDGET(togglebutton));
}


void on_protocol_optionmenu_changed(GtkOptionMenu *optionmenu, gpointer user_data) {
    rulewin_set_sensitive(GTK_WIDGET(optionmenu));
}


void on_port_spinbutton_value_changed(GtkSpinButton *spinbutton, gpointer user_data) {
    GtkWidget *w = GTK_WIDGET(spinbutton);
    struct servent *se;
    guint16 port;
    gchar *p;
    static gchar txt[256];

    port = (guint16) gtk_spin_button_get_value(GTK_SPIN_BUTTON(lookup_widget(w, "port_spinbutton")));

    if ((se = getservbyport(htons(port), NULL)))
        snprintf(p = txt, sizeof(txt), "<b>%s</b>", se->s_name);
    else
        p = "<i>Unknown</i>";

    gtk_label_set_label(GTK_LABEL(lookup_widget(w, "type_label")), p);
}


void on_match_type_checkbutton_toggled(GtkToggleButton *togglebutton, gpointer user_data) {
    rulewin_set_sensitive(GTK_WIDGET(togglebutton));
}


void on_src_netmask_checkbutton_toggled(GtkToggleButton *togglebutton, gpointer user_data) {
    rulewin_set_sensitive(GTK_WIDGET(togglebutton));
}

void on_dst_netmask_checkbutton_toggled(GtkToggleButton *togglebutton, gpointer user_data) {
    rulewin_set_sensitive(GTK_WIDGET(togglebutton));
}


void on_src_netmask_spinbutton_value_changed(GtkSpinButton *spinbutton, gpointer user_data) {
    rulewin_update_host_ranges(GTK_WIDGET(spinbutton));
}


void on_src_ip_entry_changed(GtkEditable *editable, gpointer user_data) {
    rulewin_update_host_ranges(GTK_WIDGET(editable));
}


void on_match_source_checkbutton_toggled(GtkToggleButton *togglebutton, gpointer user_data) {
    rulewin_set_sensitive(GTK_WIDGET(togglebutton));
}


void on_ok_button_clicked(GtkButton *button, gpointer user_data) {
    GtkWidget *rw = GTK_WIDGET(button);
    rulewin_ok(rw);
    gtk_widget_destroy(rw);
}


void on_cancel_button_clicked(GtkButton *button, gpointer user_data) {
    rulewin_cancel(GTK_WIDGET(button));
}


void on_add_button_clicked(GtkButton *button, gpointer user_data) {
    rulewin_show(NULL);
}


void on_desc_entry_changed(GtkEditable *editable, gpointer user_data) {
    GtkWidget *rw = GTK_WIDGET(editable);
    static gchar desc[256];
    const gchar *p;
    // Yes, the user may insert <> charachters here, but is this tragic?

    p = gtk_entry_get_text(GTK_ENTRY(lookup_widget(rw, "desc_entry")));

    if (!*p)
        p = "<span color=\"white\" size=\"xx-large\"><b><i>New rule</i></b></span>";
    
    snprintf(desc, sizeof(desc), "<span color=\"white\" size=\"xx-large\"><b>%s</b></span>", p);
    gtk_label_set_label(GTK_LABEL(lookup_widget(rw, "desc_label")), desc);
    rulewin_set_sensitive(GTK_WIDGET(editable));
}


void on_bc_radiobutton_toggled(GtkToggleButton *togglebutton, gpointer user_data) {
    rulewin_set_sensitive(GTK_WIDGET(togglebutton));
}



void on_dst_netmask_spinbutton_value_changed(GtkSpinButton *spinbutton, gpointer user_data) {
    rulewin_update_host_ranges(GTK_WIDGET(spinbutton));
}


void on_dst_ip_entry_changed(GtkEditable *editable, gpointer user_data) {
    rulewin_update_host_ranges(GTK_WIDGET(editable));
}


void on_match_destination_checkbutton_toggled(GtkToggleButton *togglebutton, gpointer user_data) {
    rulewin_set_sensitive(GTK_WIDGET(togglebutton));
}



void on_properties_button_clicked(GtkButton *button, gpointer user_data) {
    rule_t* rule;
    
    if ((rule = ruleset_get_current_rule()))
        rulewin_show(rule);
}


void on_up_button_clicked(GtkButton *button, gpointer user_data) {
    rule_t* rule;
    
    if ((rule = ruleset_get_current_rule()))
        ruleset_move_rule(rule, -1);
}


void on_down_button_clicked(GtkButton *button, gpointer user_data) {
    rule_t* rule;
    
    if ((rule = ruleset_get_current_rule()))
        ruleset_move_rule(rule, +1);
}


void on_remove_button_clicked(GtkButton *button, gpointer user_data) {
    rule_t* rule;
    
    if ((rule = ruleset_get_current_rule()))
        ruleset_remove_rule(rule);
}


void on_clear_button_clicked(GtkButton *button, gpointer user_data) {
    ruleset_clear();
}


void on_ruleset_view_cursor_changed(GtkTreeView *treeview, gpointer user_data) {
    ruleset_update_ui();
}


gboolean on_rule_window_delete_event(GtkWidget *widget, GdkEvent *event, gpointer user_data) {
    rulewin_cancel(GTK_WIDGET(widget));
    return TRUE;
}


void on_commit_button_clicked(GtkButton *button, gpointer user_data) {
    ruleset_commit();
}


void
on_save_ruleset_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_open_ruleset_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void on_advanced_button_clicked(GtkButton *button, gpointer user_data){
    advancedwin_show(TRUE);
}


gboolean on_advanced_window_delete_event(GtkWidget *widget, GdkEvent *event, gpointer user_data) {
    advancedwin_show(FALSE);
    return TRUE;
}


void on_tcp_rst_check_button_toggled(GtkToggleButton *togglebutton, gpointer user_data) {
    advancedwin_apply();
}


void on_close_button_clicked(GtkButton *button, gpointer user_data) {
    advancedwin_show(FALSE);
}


void on_ruleset_check_button_toggled(GtkToggleButton *togglebutton, gpointer user_data) {
    ruleset.ignore_rules = !gtk_toggle_button_get_active(togglebutton);
    ruleset.modified = TRUE;
    ruleset_update_ui();
}


void on_unmatch_optionmenu_changed(GtkOptionMenu *optionmenu, gpointer user_data) {
    ruleset.unmatch_verdict = gtk_option_menu_get_history(optionmenu);
    ruleset.modified = TRUE;
    ruleset_update_ui();
}

void on_new_ruleset_activate(GtkMenuItem *menuitem, gpointer user_data) {
    ruleset_new(NULL);
}

void on_about_activate(GtkMenuItem *menuitem, gpointer user_data) {
    
}

void on_log_spinbutton_value_changed(GtkSpinButton *spinbutton, gpointer user_data) {
    log_widget_cut();
}

