/*
 * DO NOT EDIT THIS FILE - it is generated by Glade.
 */

GtkWidget* create_connection_window (void);
GtkWidget* create_main_window (void);
GtkWidget* create_rule_window (void);
GtkWidget* create_wait_window (void);
GtkWidget* create_advanced_window (void);
