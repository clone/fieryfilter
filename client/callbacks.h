#include <gtk/gtk.h>


gboolean
on_connection_window_delete_event      (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_drop_button_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_reject_button_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_accept_button_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_quit_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_clear_log_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_about_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_sticky_button_toggled               (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

gboolean
on_main_window_delete_event            (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_icmp_option_menu_changed            (GtkOptionMenu   *optionmenu,
                                        gpointer         user_data);

void
on_direction_optionmenu_changed        (GtkOptionMenu   *optionmenu,
                                        gpointer         user_data);

void
on_match_direction_checkbutton_toggled (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_match_interfaces_checkbutton_toggled
                                        (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_protocol_optionmenu_changed         (GtkOptionMenu   *optionmenu,
                                        gpointer         user_data);

void
on_port_spinbutton_value_changed       (GtkSpinButton   *spinbutton,
                                        gpointer         user_data);

void
on_match_type_checkbutton_toggled      (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_src_netmask_checkbutton_toggled     (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_src_netmask_spinbutton_value_changed
                                        (GtkSpinButton   *spinbutton,
                                        gpointer         user_data);

void
on_src_ip_entry_changed                (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_match_source_checkbutton_toggled    (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_cancel_button_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_ok_button_clicked                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_add_button_clicked                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_desc_entry_changed                  (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_bc_radiobutton_toggled              (GtkToggleButton *togglebutton,
                                        gpointer         user_data);
void
on_dst_netmask_spinbutton_value_changed
                                        (GtkSpinButton   *spinbutton,
                                        gpointer         user_data);

void
on_dst_ip_entry_changed                (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_match_destination_checkbutton_toggled
                                        (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_dst_netmask_checkbutton_toggled     (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_properties_button_clicked           (GtkButton       *button,
                                        gpointer         user_data);

void
on_up_button_clicked                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_down_button_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_remove_button_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_clear_button_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_ruleset_view_cursor_changed         (GtkTreeView     *treeview,
                                        gpointer         user_data);

gboolean
on_rule_window_delete_event            (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_commit_button_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_save_ruleset_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_open_ruleset_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_advanced_button_clicked             (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_advanced_window_delete_event        (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_tcp_rst_check_button_toggled        (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_close_button_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_ruleset_check_button_toggled        (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_unmatch_optionmenu_changed          (GtkOptionMenu   *optionmenu,
                                        gpointer         user_data);

void
on_new_ruleset_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_log_spinbutton_value_changed        (GtkSpinButton   *spinbutton,
                                        gpointer         user_data);
