#include <sys/syscall.h>
#include <mcheck.h>

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>

#include "interface.h"
#include "support.h"
#include "connection.h"
#include "daemon.h"
#include "mainwin.h"
#include "ruleset.h"

int main (int argc, char *argv[]) {
    mtrace();
    
#ifdef ENABLE_NLS
    bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
    bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
    textdomain (GETTEXT_PACKAGE);
#endif
    
    gtk_set_locale();
    gtk_init(&argc, &argv);
    
    add_pixmap_directory (PACKAGE_DATA_DIR "/" PACKAGE "/pixmaps");
    
    if (daemon_init() < 0)
        goto finish;

    if (ruleset_init() < 0)
        goto finish;

    if (ruleset_initial_load() < 0)
        goto finish;

    if (ruleset_commit() < 0)
        goto finish;

    mainwin_show();
    gtk_main ();
    
finish:
    ruleset_done();
    daemon_done();

    //syscall(SYS_exit, 0);
    
    return 0;
}


