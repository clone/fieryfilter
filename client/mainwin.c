#include <time.h>
#include <stdio.h>

#include "mainwin.h"
#include "interface.h"
#include "support.h"
#include "connection.h"
#include "log.h"
#include "ruleset.h"
#include "format.h"
#include "advancedwin.h"

GtkWidget* get_main_window(void) {
    static GtkWidget *mw = NULL;

    if (!mw) {
        GdkColor color;
        mw = create_main_window();
        gdk_color_parse ("black", &color);
        gtk_widget_modify_bg(lookup_widget(mw, "title_eventbox"), GTK_STATE_NORMAL, &color);
        log_widget_init();
        ruleset_widget_init();

        gtk_label_set_label(GTK_LABEL(lookup_widget(mw, "version_label")), "<i><span color=\"white\">Version "VERSION"</span></i>");
    }

    return mw;
}

void mainwin_show() {
    gtk_widget_show_all(get_main_window());
}

void mainwin_update_status_bar() {
    GtkStatusbar *s;
    static guint ctx = (guint) -1;
    static gchar txt[256];

    s = GTK_STATUSBAR(lookup_widget(get_main_window(), "statusbar"));
    
    if (ctx == (guint) -1) 
        ctx = gtk_statusbar_get_context_id(s, "Recieved packets");
    else
        gtk_statusbar_pop(s, ctx);

    if (queued_conn_count || conn_current)
        snprintf(txt, sizeof(txt), "Recieved %u packets, %u oustanding", total_conn_count, queued_conn_count + (conn_current ? 1 : 0));
    else
        snprintf(txt, sizeof(txt), "Recieved %u packets", total_conn_count);
    
    gtk_statusbar_push(s, ctx, txt);
}
