#ifndef fooadvancedwinhfoo
#define fooadvancedwinhfoo

#include <glib.h>

void advancedwin_show(gboolean b);
void advancedwin_fill();
void advancedwin_apply();

#endif
