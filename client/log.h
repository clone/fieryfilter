#ifndef foologhfoo
#define foologhfoo

#include "connection.h"

void log_widget_init();
void log_widget_append(conn_info_t *ci);
void log_widget_verdict(conn_info_t *ci, verdict_t v);
void log_widget_clear();
void log_widget_cut();

#endif
