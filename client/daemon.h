#ifndef foodaemonhfoo
#define foodaemonhfoo

#include <glib.h>
#include <linux/netfilter.h>
#include <libipq/libipq.h>

int daemon_init();
void daemon_done();

void daemon_verdict(unsigned long id, guint r);

#endif
