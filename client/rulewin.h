#ifndef foorulewinhfoo
#define foorulewinhfoo

#include <gtk/gtk.h>

#include "ruleset.h"

int rulewin_show(rule_t *rule);
void rulewin_set_sensitive(GtkWidget *rw);
void rulewin_ok(GtkWidget *rw);
void rulewin_cancel(GtkWidget *rw);
void rulewin_update_host_ranges(GtkWidget *rw);

#endif
