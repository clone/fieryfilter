#include "mainwin.h"
#include "support.h"

#include "log.h"

static GtkListStore *log_list_store = NULL;
enum { COLUMN_TIME, COLUMN_TYPE, COLUMN_SOURCE, COLUMN_DESTINATION, COLUMN_DECISION, N_COLUMNS };

void log_widget_init() {
    GtkTreeView *tv = GTK_TREE_VIEW(lookup_widget(get_main_window(), "log_view"));

    log_list_store = gtk_list_store_new(N_COLUMNS, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
    gtk_tree_view_set_model(tv, GTK_TREE_MODEL(log_list_store));

    gtk_tree_view_append_column(tv, gtk_tree_view_column_new_with_attributes("Time", gtk_cell_renderer_text_new(), "text", COLUMN_TIME, NULL));
    gtk_tree_view_append_column(tv, gtk_tree_view_column_new_with_attributes("Type", gtk_cell_renderer_text_new(), "text", COLUMN_TYPE, NULL));
    gtk_tree_view_append_column(tv, gtk_tree_view_column_new_with_attributes("Source", gtk_cell_renderer_text_new(), "text", COLUMN_SOURCE, NULL));
    gtk_tree_view_append_column(tv, gtk_tree_view_column_new_with_attributes("Destination", gtk_cell_renderer_text_new(), "text", COLUMN_DESTINATION, NULL));
    gtk_tree_view_append_column(tv, gtk_tree_view_column_new_with_attributes("Decision", gtk_cell_renderer_text_new(), "text", COLUMN_DECISION, NULL));
}


void log_widget_append(conn_info_t *ci) {
    gtk_list_store_append(log_list_store, &ci->iter);
    gtk_list_store_set(log_list_store, &ci->iter,
                       COLUMN_TIME, ci->timestamp_string,
                       COLUMN_TYPE, ci->type_string, 
                       COLUMN_SOURCE, ci->from_string,
                       COLUMN_DESTINATION, ci->to_string,
                       COLUMN_DECISION, "outstanding...",
                       -1);

    log_widget_cut();
    
    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(lookup_widget(get_main_window(), "autoscroll_button")))) {
        GtkTreeView *tv = GTK_TREE_VIEW(lookup_widget(get_main_window(), "log_view"));
        GtkTreePath *path;

        path = gtk_tree_model_get_path(gtk_tree_view_get_model(tv), &ci->iter);
        gtk_tree_view_set_cursor(tv, path, NULL, FALSE);
        gtk_tree_view_scroll_to_cell(tv, path, NULL, TRUE, .5, 0);
        gtk_tree_path_free(path);
    }
}

void log_widget_verdict(conn_info_t *ci, verdict_t v) {
    gtk_list_store_set(log_list_store, &ci->iter,
                       COLUMN_DECISION, v == VERDICT_REJECT ? "REJECTED" : (v == VERDICT_DROP ? "DROPPPED" : "ACCEPTED"),
                       -1);
}

void log_widget_clear() {
    gtk_list_store_clear(log_list_store);
}


void log_widget_cut() {
    gfloat m = gtk_spin_button_get_value(GTK_SPIN_BUTTON(lookup_widget(get_main_window(), "log_spinbutton")));

    if (m < 10)
        m = 10;

    while (gtk_tree_model_iter_n_children(GTK_TREE_MODEL(log_list_store), NULL) > m) {
        GtkTreeIter iter;
        if (!gtk_tree_model_get_iter_first(GTK_TREE_MODEL(log_list_store), &iter))
            return;
        
        gtk_list_store_remove(log_list_store, &iter);
    }
    
}
