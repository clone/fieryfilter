#ifndef fooformathfoo
#define fooformathfoo

#include <glib.h>
#include <gtk/gtk.h>

#include "main.h"

typedef struct icmp_entry {
    gchar *name;
    guint8 value;
} icmp_entry_t;

typedef enum verdict_format {
    FORMAT_USER,
    FORMAT_XML
} verdict_format_t;

extern icmp_entry_t icmp_type_table[];
extern icmp_entry_t icmp_code_table[];

gchar* icmp_type_str(guint8 t);
gchar* icmp_code_str(guint8 c);
void fill_icmp_menu(GtkOptionMenu *om, gboolean w, guint8 def);
void set_icmp_menu_entry(GtkOptionMenu *om, gboolean w, guint8 def);
gchar* format_hostname(guint32 a);
gchar* format_ip_address(guint32 a);
GList *fill_interface_list();
void free_interface_list(GList *l);
gboolean is_broadcast(guint32 ip);
gchar* format_host_range(const gchar *ip, gint bits);
gchar* format_verdict(verdict_t v, verdict_format_t t);

#endif
