#ifndef foorulehfoo
#define foorulehfoo

#include <net/if.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <libxml/tree.h>

#include "connection.h"
#include "main.h"

typedef enum match {
    MATCH_INTERFACES = 1,
    MATCH_DIRECTION = 4,
    MATCH_TYPE = 8,
    MATCH_SOURCE = 16,
    MATCH_DESTINATION = 32,
    MATCH_BROADCAST = 64,
    MATCH_UNICAST = 128
} match_t;

typedef struct rule {
    gboolean realized;
    gboolean enabled;
    gboolean being_edited;
    verdict_t verdict;
    char description[64];
    guint32 id;
    GtkTreeIter iter;
    
    match_t match;
    char device_in[IFNAMSIZ+1];
    char device_out[IFNAMSIZ+1];
    conn_direction_t direction;
    guint protocol;
    guint port;
    guint icmp_type;
    guint32 src_ip_address;
    guint src_netmask_bits;
    guint32 dst_ip_address;
    guint dst_netmask_bits;
} rule_t;

rule_t* rule_new();
rule_t* rule_new_from_conn_info(conn_info_t *ci);
rule_t* rule_new_from_xml(xmlDocPtr doc, xmlNodePtr node);
void rule_free(rule_t *rule);
gchar* rule_match_string(rule_t* rule);

int rule_to_xml(rule_t*rule, xmlDocPtr doc, xmlNodePtr node);

#endif
