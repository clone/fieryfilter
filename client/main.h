#ifndef foomainhfoo
#define foomainhfoo

typedef enum verdict { VERDICT_QUERY, VERDICT_ACCEPT, VERDICT_REJECT, VERDICT_DROP } verdict_t;
typedef enum conn_direction { DIR_INCOMING, DIR_OUTGOING, DIR_PASSING } conn_direction_t;

#endif
