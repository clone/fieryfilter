#ifndef fooconnectionhfoo
#define fooconnectionhfoo

#include <glib.h>
#include <linux/netfilter.h>
#include <libipq/libipq.h>
#include <gtk/gtk.h>

#include "../daemon/common.h"
#include "main.h"

typedef struct conn_info {
    unsigned long id;
    conn_direction_t direction;
    gchar device_in[IFNAMSIZ+1];
    gchar device_out[IFNAMSIZ+1];
    guint32 src_ip_address, dst_ip_address;
    guint protocol;
    guint16 port;
    guint8 icmp_type;
    long timestamp;
    gboolean broadcast;
    
    gchar* from_string;
    gchar* to_string;
    gchar* type_string;
    gchar* timestamp_string;
    gchar* port_string;

    GtkTreeIter iter;
} conn_info_t;

void conn_new(ipq_packet_msg_t *ci);
void conn_verdict(verdict_t v);
void conn_set_sticky();

extern guint total_conn_count;
extern guint queued_conn_count;
extern conn_info_t *conn_current;

#endif
