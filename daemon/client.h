#ifndef fooclienthfoo
#define fooclienthfoo

#include "common.h"

int client_init();
void client_done();
int client_work();
int client_is_connected();
int client_send_enqueue(message_t *m);
message_t* message_new(message_code_t c, guint8* d, guint s);

#endif
