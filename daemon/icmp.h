#ifndef fooipsumhfoo
#define fooipsumhfoo

#include <glib.h>
#include <libipq/libipq.h>

int reply_icmp_error(ipq_packet_msg_t *m, int code);
int icmp_init();
void icmp_done();

#endif
