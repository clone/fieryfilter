#ifndef foomainhfoo
#define foomainhfoo

#include <sys/select.h>

extern fd_set listen_wfds, listen_rfds, select_wfds, select_rfds;

extern gboolean fail;

extern guint32 default_verdict;

extern gboolean log_packets;

#endif
