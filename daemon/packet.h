#ifndef foopackethfoo
#define foopackethfoo

#include <libipq/libipq.h>
#include <glib.h>

void packet_new(ipq_packet_msg_t *m);
ipq_packet_msg_t* packet_find(unsigned long id);
void packet_release(unsigned long id);
void packet_foreach(gboolean age, gboolean (*func)(ipq_packet_msg_t*));
void packets_free();
guint get_queued_packets();
guint get_total_packets();

#endif
