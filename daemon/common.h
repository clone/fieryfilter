#ifndef foocommonhfoo
#define foocommonhfoo

#include <sys/types.h>
#include <glib.h>
#include <libipq/libipq.h>

#define SOCKET_PATH "/tmp/fieryfilter"

typedef enum message_code {
    MSG_PACKET = 0,
    MSG_VERDICT = 1,
    MSG_SET_DEFAULT_VERDICT = 2
} message_code_t;

typedef struct message {
    message_code_t code;
    pid_t pid;
    guint32 length;
} message_t;


#endif
