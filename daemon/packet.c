#include <time.h>

#include "packet.h"

#define PACKET_TIMEOUT 30

typedef struct packet_wrapper {
    time_t timestamp;
    ipq_packet_msg_t m;
} packet_wrapper_t;

static GSList *packets = NULL;
static guint n_packets = 0;
static guint total_packets = 0;

guint get_queued_packets() {
    return n_packets;
}

guint get_total_packets() {
    return total_packets;
}

void packet_new(ipq_packet_msg_t *m) {
    packet_wrapper_t *w = (packet_wrapper_t*) g_malloc(sizeof(packet_wrapper_t)+ m->data_len);
    memcpy(&w->m, m, sizeof(ipq_packet_msg_t)+ m->data_len);
    time(&w->timestamp);

    packets = g_slist_prepend(packets, w);
    n_packets++;
    total_packets++;
}

static packet_wrapper_t* _find(unsigned long id) {
    GSList *l;
    
    for (l = packets; l; l = l->next) {
        packet_wrapper_t *w = (packet_wrapper_t*) l->data;
        if (w->m.packet_id == id)
            return w;
    }

    return NULL;
}

ipq_packet_msg_t* packet_find(unsigned long id) {
    packet_wrapper_t* w;

    if ((w = _find(id)))
        return &w->m;
    
    return NULL;
}

void packet_release(unsigned long id) {
    packet_wrapper_t *w;

    w = _find(id);
    g_assert(w);
    
    packets = g_slist_remove(packets, w);
    g_free(w);
    n_packets--;
}


void packet_foreach(gboolean age, gboolean (*func)(ipq_packet_msg_t*)) {
    GSList *l, *p;

    for (l = packets, p = NULL; l;) {
        gboolean remove = FALSE;
        packet_wrapper_t *w = (packet_wrapper_t*) l->data;
        
        if (!age || w->timestamp + PACKET_TIMEOUT < time(NULL))
            if (!func(&w->m))
                remove = TRUE;

        
        
        if (remove) {
            if (p)
                p->next = l->next;
            else
                packets = l->next;
            
            g_slist_free_1(l);

            l = p;
        } else
            l = l->next;
    }
}

void packets_free() {
    while (packets) {
        packet_wrapper_t *w;
        w = (packet_wrapper_t*) packets->data;
        packets = g_slist_remove(packets, w);
        g_free(w);
    }
}
