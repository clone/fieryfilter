#ifndef fooipqapihfoo
#define fooipqapihfoo

#include <linux/netfilter.h>
#include <libipq/libipq.h>
#include <glib.h>

int ipqapi_verdict(ipq_packet_msg_t *m, guint32 resp);
int ipqapi_work();
void ipqapi_done();
int ipqapi_init();

#endif
